// see https://docs.renovatebot.com/configuration-options

module.exports = {
  "platform": "gitlab",
  "endpoint": "https://gitlab.com/api/v4/",
  "assignees": ["LucaVazz"],
  "rebaseWhen": "auto",
  "fetchReleaseNotes": false,
  "semanticCommits": "enabled",
  "prBodyTemplate": "{{{header}}} {{{table}}} --- {{{notes}}} {{{controls}}} --- {{{footer}}}",
  "repositories": ["LucaVazz/patcher", "LucaVazz/music-x"],
  "extends": ["config:base", ":pinAllExceptPeerDependencies"]
}
